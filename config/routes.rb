Rails.application.routes.draw do
  resources :users, :products
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  post '/auth/login', to: 'authentication#login'
end
