ActiveAdmin.register Product do
  permit_params :category, :name, :description, :stock, :price, :category_id, :image
  form do |f|
    f.inputs 'Product Details' do
      f.input :category
      f.input :name
      f.input :description
      f.input :price
      f.input :stock
      f.input :image, as: :file
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
      row :description
      row :price
      row :stock
      row :image do |product|
        image_tag product.image if product.image.attached?
      end
    end
  end

  filter :name
  filter :description
  filter :price

  filter :image_attachment_id, label: 'Image ID',
                               as: :string,
                               collection: -> { Product.with_attached_image.map { |p| [p.image.blob.id] } }

  index do
    selectable_column
    column :name
    column :description
    column :price
    column :stock
    column :image do |product|
      image_tag product.image.variant(resize: '2x2') if product.image.attached?
    end
    actions
  end
end
