ActiveAdmin.register Cart do
  permit_params :category, :name, :description, :stock, :price
  show do
    panel 'Cart Items' do
      table_for cart.cart_items do
        column('Product') { |item| item.product.name }
        column('Quantity') { |item| item.quantity }
      end
    end
  end
end
