ActiveAdmin.register Category do
  permit_params :name
  form do |f|
    f.inputs 'Product Details' do
      f.input :name
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
    end
  end

  filter :name

  index do
    selectable_column
    column :name
    actions
  end
end
