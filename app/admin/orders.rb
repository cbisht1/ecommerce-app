ActiveAdmin.register Order do
  permit_params :user_id, :shipping_address, :name, :total_amount
  form do |f|
    f.inputs 'Order Details' do
      f.input :user
      f.input :shipping_address
      f.input :total_amount
    end
    f.actions
  end

  show do
    attributes_table do
      row :user
      row :shipping_address
      row :total_amount
    end
  end

  filter :user

  index do
    selectable_column
    column :user
    column :shipping_address
    column :total_amount
    actions
  end
end
