ActiveAdmin.register Review do
  permit_params :user, :product, :title, :description, :rating
end
