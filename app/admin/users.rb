ActiveAdmin.register User do
  permit_params :name, :email, :password, :mobile_no
  form do |f|
    f.inputs 'Order Details' do
      f.input :name
      f.input :email
      f.input :password
      f.input :mobile_no
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
      row :email
      row :password
      row :mobile_no
    end
  end

  filter :user

  index do
    selectable_column
    column :name
    column :email
    column :password
    column :mobile_no
    actions
  end
end
