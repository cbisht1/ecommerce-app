class Product < ApplicationRecord
  belongs_to :category

  has_one_attached :image

  def self.ransackable_attributes(_auth_object = nil)
    %w[created_at description id name price updated_at items image_attachment_id_cont image_attachment_id_eq
       image_attachment_id_start image_attachment_id_end]
  end

  def self.ransackable_associations(_auth_object = nil)
    ['cart_item']
  end
end
