class Order < ApplicationRecord
  
  belongs_to :user
  def self.ransackable_attributes(_auth_object = nil)
    %w[created_at id shipping_address total_amount updated_at user_id]
  end
end
