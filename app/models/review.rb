class Review < ApplicationRecord

  belongs_to :user
  belongs_to :product
  def self.ransackable_attributes(_auth_object = nil)
    %w[created_at description id product_id rating title updated_at user_id]
  end
end
