class User < ApplicationRecord

  has_one :cart
  has_one :product
  has_one :order
  has_one :cart_items
  has_one :category

  has_secure_password

  validates :name, presence: true
  validates :email, presence: true, uniqueness: true
  validates :password, presence: true, length: { minimum: 8 }, on: :create
end
