class Cart < ApplicationRecord
  belongs_to :user

  has_many :cart_items
  has_many :products, through: :cart_items

  def add_product(product, quantity = 1)
    cart_item = cart_items.find_or_initialize_by(product_id: product.id)
    cart_item.quantity += quantity
    cart_item.save
  end
end
