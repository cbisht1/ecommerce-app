class ProductSerializer
  include FastJsonapi::ObjectSerializer
  attributes :category, :name, :description, :stock, :price 
end
