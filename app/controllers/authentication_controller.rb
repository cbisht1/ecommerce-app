class AuthenticationController < ApplicationController
  skip_before_action :authenticate_user

  def login
    @user = User.find_by(email: params[:email])
    if @user && user_authenticated?
      render_success_response
    else
      render_unauthorized_response
    end
  end

  private

  def user_authenticated?
    @user&.authenticate(params[:password])
  end

  def render_success_response
    payload = { user_id: @user.id }
    token = JwtToken.jwt_encode(payload)

    render json: {
      token: token,
      exp: (Time.now + 24.hours).strftime('%m-%d-%Y %H:%M'),
      name: @user.name
    }, status: :ok
  end

  def render_unauthorized_response
    render json: { error: 'unauthorized' }, status: :unauthorized
  end
end
