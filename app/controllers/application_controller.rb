class ApplicationController < ActionController::Base
  include JwtToken

  before_action :authenticate_user, unless: :active_admin_request?

  private

  rescue_from ActiveRecord::RecordNotFound, with: :handle_record_not_found

  def authenticate_user
    header = request.headers['Authorization']
    header = header.split('').last if header
    begin
      @decoded = JwtToken.jwt_decode(header)
      @current_user = User.find(@decoded[:user_id])
    rescue ActiveRecord::RecordNotFound => e
      render json: { errors: e.message }, status: :unauthorized
    rescue JWT::DecodeError => e
      render json: { errors: e.message }, status: :unauthorized
    end
  end

  def active_admin_request?
    request.path.include?('/admin')
  end
end
