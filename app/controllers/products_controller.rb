class ProductsController < ApplicationController
  skip_before_action :authenticate_user, only: %i[show index]
  before_action :set_product, only: :show

  def index
    @products = Product.all
    render json: ProductSerializer.new(@products)
  end

  def show
    render json: ProductSerializer.new(@product)
  end

  private

  def product_params
    params.require(:product).permit(:name, :description, :price)
  end

  def set_product
    @product = Product.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    render json: {
      message: 'Product does not exist', status: 'Error'
    }, status: :not_found
  rescue StandardError => e
    render json: {
      message: 'Unable to fetch product', error: e.message, status: 'Error'
    }, status: :internal_server_error
  end
end
