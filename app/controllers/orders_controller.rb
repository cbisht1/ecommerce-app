class OrderController < ApplicationController
  skip_before_action :authenticate_user, only: %i[show index]
  before_action :set_order, only: :show

  def index
    @order = Order.all
    render json: ProductSerializer.new(@order)
  end

  def show
    render json: ProductSerializer.new(@order)
  end

  private

  def product_params
    params.require(:order).permit(:name, :email, :password, :mobile_no)
  end

  def set_order
    @order = Order.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    render json: {
      message: 'Product does not exist', status: 'Error'
    }, status: :not_found
  end
end
