require 'rails_helper'

RSpec.describe Order, type: :model do
  describe 'associations' do
    specify { is_expected.to belong_to(:user) }
  end
end
