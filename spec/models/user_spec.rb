require 'rails_helper'

RSpec.describe User, type: :model do
  
  describe 'Validations' do
    specify { is_expected.to validate_presence_of(:name) }
    specify { is_expected.to validate_presence_of(:email) }
    specify { is_expected.to validate_uniqueness_of(:email) }
    specify { is_expected.to allow_value('user@example.com').for(:email) }
    specify { is_expected.to_not allow_value('invalid_email').for(:email) }
    specify { is_expected.to validate_presence_of(:password) }
    specify { is_expected.to validate_length_of(:password).is_at_least(8) }
    specify { is_expected.to_not validate_presence_of(:mobile_no) }
    specify { is_expected.to allow_value('1234567890').for(:mobile_no) }
    specify { is_expected.to_not allow_value('123').for(:mobile_no) }
  end
end
