require 'rails_helper'

RSpec.describe CartItem, type: :model do
  describe 'associations' do
    specify { is_expected.to belong_to(:cart) }
    specify { is_expected.to belong_to(:product) }
  end
end
