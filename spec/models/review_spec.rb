require 'rails_helper'

RSpec.describe Review, type: :model do
  describe 'associations' do
    specify { is_expected.to belong_to(:user) }
    specify { is_expected.to belong_to(:product) }
  end
end
