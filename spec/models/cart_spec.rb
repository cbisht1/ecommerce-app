require 'rails_helper'

RSpec.describe Cart, type: :model do
  describe 'associations' do
    specify { is_expected.to belong_to(:user) }
  end
end
