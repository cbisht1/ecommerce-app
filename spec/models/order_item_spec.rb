require 'rails_helper'

RSpec.describe OrderItem, type: :model do
  describe 'associations' do
    specify { is_expected.to belong_to(:order) }
    specify { is_expected.to belong_to(:product) }
  end
end
