require 'rails_helper'

RSpec.describe Product, type: :model do
  describe 'associations' do
    specify { is_expected.to belong_to(:category) }
  end
end
