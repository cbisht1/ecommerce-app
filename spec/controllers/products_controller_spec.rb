require 'rails_helper'

RSpec.describe ProductsController, type: :controller do
  describe 'GET #index' do
    let!(:category) { create(:category) }
    let!(:product) do
      category.products.create(name: Faker::Name.name)
    end
    it 'returns a successful response' do
      get :index
      expect(response).to have_http_status(:success)
    end

    it 'renders JSON with products' do
      get :index
      expect(response).to have_http_status(:success)
      expect(response.content_type).to eq('application/json; charset=utf-8')
      json_response = JSON.parse(response.body)
      expect(response.body).to include(product.name)
    end
  end

  describe 'GET #show' do
    let(:category) { create(:category) }
    let(:product) do
      category.products.create(name: Faker::Name.name)
    end
    it 'returns a successful response' do
      get :show, params: { id: product.id }
      expect(response).to have_http_status(:success)
    end

    it 'renders JSON with the product' do
      get :show, params: { id: product.id }
      expect(response).to have_http_status(:success)
      expect(response.content_type).to eq('application/json; charset=utf-8')
      expect(response.body).to include(product.name)
    end

    it 'returns a not found response for an invalid product id' do
      get :show, params: { id: 'invalid_id' }
      expect(response).to have_http_status(:not_found)
      expect(response.content_type).to eq('application/json; charset=utf-8')
      expect(JSON.parse(response.body)).to include('message' => 'Product does not exist', 'status' => 'Error')
    end
  end
end
