require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  let(:valid_attributes) { { name: 'testuser', email: 'test@example.com', password: 'password' } }
  let(:invalid_attributes) { { name: '', email: '', password: '' } }
  let!(:user) { create(:user) }

  describe 'GET #index' do
    it 'returns a success response' do
      get :index
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      get :show, params: { id: user.to_param }
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST #create' do
    context 'with valid parameters' do
      it 'creates a new User' do
        expect do
          post :create, params: { user: valid_attributes }
        end.to change(User, :count).by(1)
      end

      it 'renders a JSON response with the new user and status 201' do
        post :create, params: { user: valid_attributes }
        expect(response).to have_http_status(:created)
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new User' do
        expect do
          post :create, params: { user: invalid_attributes }
        end.not_to change(User, :count)
      end

      it 'renders a JSON response with errors and status 503' do
        post :create, params: { user: invalid_attributes }
        expect(response).to have_http_status(:service_unavailable)
        expect(JSON.parse(response.body)).to have_key('errors')
      end
    end
  end

  describe 'PATCH #update' do
    context 'with valid parameters' do
      let(:new_attributes) { { name: 'new_username' } }

      it 'updates the requested user' do
        patch :update, params: { id: user.id, user: new_attributes }
        user.reload
        expect(user.name).to eq('new_username')
      end

      it 'renders a JSON response with the user and status 200' do
        patch :update, params: { id: user.to_param, user: valid_attributes }
        expect(response).to have_http_status(:no_content)
        expect(response.body).to be_empty
      end
    end

    context 'with invalid parameters' do
      it 'does not update the user' do
        original_user = user.name
        patch :update, params: { id: user.to_param, user: invalid_attributes }
        user.reload
        expect(user.name).to eq(original_user)
      end

      it 'renders a JSON response with errors and status 503' do
        patch :update, params: { id: user.to_param, user: invalid_attributes }
        expect(response).to have_http_status(:service_unavailable)
        expect(response.content_type).to eq('application/json; charset=utf-8')
        expect(JSON.parse(response.body)).to have_key('errors')
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested user' do
      expect do
        delete :destroy, params: { id: user.to_param }
      end.to change(User, :count).by(-1)
    end
  end
end
