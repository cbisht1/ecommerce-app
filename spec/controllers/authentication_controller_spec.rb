require 'rails_helper'

RSpec.describe AuthenticationController, type: :controller do
  describe 'POST #login' do
    let(:user) { create(:user, email: 'user@example.com', password: 'password') }

    context 'with valid credentials' do
      it 'returns a JWT token and user details' do
        post :login, params: { email: user.email, password: 'password' }
        expect(response).to have_http_status(:ok)

        response_json = JSON.parse(response.body)
        expect(response_json).to have_key('token')
        expect(response_json).to have_key('exp')
        expect(response_json).to have_key('name')
      end
    end

    context 'with invalid credentials' do
      it 'returns unauthorized' do
        post :login, params: { email: 'invalid@example.com', password: 'wrong_password' }
        expect(response).to have_http_status(:unauthorized)
        response_json = JSON.parse(response.body)
        expect(response_json).to eq({ 'error' => 'unauthorized' })
      end
    end
  end
end
