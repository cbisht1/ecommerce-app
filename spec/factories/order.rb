FactoryBot.define do
  factory :order do
    shipping_address { Faker::Address.street_address }
    description { Faker::Lorem.paragraph }
    total_amount { Faker::Number.number }
  end
end
