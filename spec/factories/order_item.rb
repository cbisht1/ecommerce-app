FactoryBot.define do
  factory :order_item do
    quantity { Faker::Number.number }
  end
end
