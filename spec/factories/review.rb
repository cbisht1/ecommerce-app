FactoryBot.define do
  factory :review do
    title { Faker::Name.title }
    description { Faker::Lorem.paragraph }
    rating { Faker::Number.number }
  end
end
