FactoryBot.define do
  factory :cart_items do
    quantity { Faker::Number.number }
  end
end
