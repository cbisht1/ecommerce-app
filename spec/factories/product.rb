FactoryBot.define do
  factory :product do
    name { Faker::Name.name }
    description { Faker::Lorem.paragraph }
    stock { Faker::Number.number }
    price { Faker::Number.number }
  end
end
